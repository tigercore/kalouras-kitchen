<?php include('perch/runtime.php');?>

<?php require_once('includes/head.php'); ?>
<body id="menus">
  
<?php require_once('includes/header.php'); ?>
  <div id="main" role="main">

    <div id="content">

    <h2>Sample Menus</h2>

    <img class="banner" src="img/menus.png">


      <ul class="tabs">
        <li><a href="#">Spring/Summer Menu</a></li>
        <li><a href="#">Aumtumm/Winter</a></li>
        <li><a href="#">Canapé menu</a></li>
        <li><a href="#">A few favourites</a></li> 
      </ul>

<!-- tab "panes" -->
<div class="panes">
  <div class="spring-summer">
		
		<h3 class="menu-course">Starters</h3>
  	<ul class="menu-course-choices">
  		<?php perch_content('Spring/Summer Starters'); ?>
  	</ul>

  	<h3 class="menu-course">Main Courses</h3>
  	  	<ul class="menu-course-choices">
  		<?php perch_content('Spring/Summer Mains'); ?>
  	</ul>

  	<h3 class="menu-course">Desserts</h3>
  	  	<ul class="menu-course-choices">
  		<?php perch_content('Spring/Summer Desserts'); ?>
  	</ul>

  </div>
  
  <div class="autumn-winter">
  	<h3 class="menu-course">Starters</h3>
  	<ul class="menu-course-choices">
  		<?php perch_content('Autumn/Winter Starters'); ?>
  	</ul>

  	<h3 class="menu-course">Main Courses</h3>
  	  	<ul class="menu-course-choices">
  		<?php perch_content('Autumn/Winter Mains'); ?>
  	</ul>

  	<h3 class="menu-course">Desserts</h3>
  	  	<ul class="menu-course-choices">
  		<?php perch_content('Autumn/Winter Desserts'); ?>
  	</ul>

  </div>
  <div class="canape">

  	  	<h3 class="menu-course">Cold</h3>
  	  	<ul class="menu-course-choices">
  		<?php perch_content('Canapé Cold'); ?>
  	</ul>
  
   <h3 class="menu-course">Hot</h3>
  	<ul class="menu-course-choices">
  		<?php perch_content('Canapé Hot'); ?>
  	</ul>



  	<h3 class="menu-course">Sweet</h3>
  	  	<ul class="menu-course-choices">
  		<?php perch_content('Canapé Sweet'); ?>
  	</ul>


<p class="note">The canap&eacute; menu is priced at &pound;10.50 per person for a choice of 10 canap&eacute;s. We are also able to provide serving staff &ndash; quotes are available on request.</p>




      
  </div>

  <div class="favourites">
  	   <h3 class="menu-course">Starters</h3>
  	<ul class="menu-course-choices">
  		<?php perch_content('Favourites Starters'); ?>
  	</ul>

  	<h3 class="menu-course">Mains</h3>
  	  	<ul class="menu-course-choices">
  		<?php perch_content('Favourites Mains'); ?>
  	</ul>

  	<h3 class="menu-course">Desserts</h3>
  	  	<ul class="menu-course-choices">
  		<?php perch_content('Favourites Desserts'); ?>
  	</ul>




</div>






    </div>
  </div>
  
  <?php require_once('includes/footer.php'); ?>

  <?php require_once('includes/scripts.php'); ?>


  
</body>
</html>