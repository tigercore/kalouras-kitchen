<?php include('perch/runtime.php');?>

<?php require_once('includes/head.php'); ?>
<body id="prices">
  
<?php require_once('includes/header.php'); ?>
  <div id="main" role="main">

    <div id="content">

    <h2>Prices</h2>

    <img class="banner" src="img/prices.png">

    <p><?php perch_content('Intro'); ?></p>

      

      <table>

        <tr>
          <td>First four hours (minimum)</td>
          <td><?php perch_content('First four hours'); ?></td>
        </tr>
        <tr class="odd">
          <td>Each subsequent hour</td>
          <td><?php perch_content('Each subsequent hour'); ?></td>
        <tr>
        </tr>
          <td>Waiting staff (per person)</td>
          <td><?php perch_content('Waiting staff'); ?></td>
        </tr>
        <tr class="odd">
          <td>Travel expenses</td>
          <td>Dependent on location</td>
        </tr>

      </table>

<h4>Food costs are in addition to Chef &amp; Waiter charges.</h4>
<p>Prices vary depending on what menu you choose. We can provide a rough estimation for food cost. The final cost is according to receipts of shopping that can be provided.</p>
<p>A deposit of 50% of the Total (including waitress fee) is required no later than 7 days before the event.</p>
<h4>Changes &amp; Cancellation Policy</h4>
<p>Some of the products such as meat products must be order about a week before the event. Any changes or cancellations will be received with minimum 8 days notice. (We will contact you a day before we order the products). Any cancellation later than this might result with cancellation fee of 50%.
Availability can be quite restricted, especially in summer so please book early, particularly in the Rock, Polzeath and Padstow area.
</p>
 
    </div>
  </div>
  
  <?php require_once('includes/footer.php'); ?>

  <?php require_once('includes/scripts.php'); ?>


  
</body>
</html>