<?php require_once('includes/head.php'); ?>
<body id="faq">
  
<?php require_once('includes/header.php'); ?>
  <div id="main" role="main">

    <div id="content">

    <h2>Frequently Asked Questions</h2>

    <img class="banner" src="img/faqs.png">

      <dl>

        <dt>How does the service work?</dt>

        <dd>Georgie will start with a free (and no-obligation) consultation in the client's home to learn their culinary likes, dislikes and wishes but also to take a quick tour of the kitchen.  You are free to contact her with any questions between booking and the day itself.</dd>


        <dt>Where are the meals prepared?</dt>

        <dd>Georgie will prepare all meals in your kitchen. She will bring the fresh ingredients for your meals along with any necessary pots, pans and utensils, and prepare your food on site. At the end of the day she will leave your kitchen as she found it.</dd>


        <dt>Do we specialise in any type of cooking?</dt>

        <dd>Georgie cooks a wide variety of food, especially Mediterranean and Thai.</dd>

        <dt>Do we take into account special dietary needs?</dt>

        <dd>Yes. When going over your food preferences, Georgie will find out if you or your guests have any special dietary restrictions. The menus that we customise for you will incorporate all of your needs. For those guests with nut allergies however we cannot guarantee that our meals are completely nut free.</dd>


        <dt>Who uses our Personal Chef Service?</dt>

        <dd>A common misconception is that a personal chef service is only for the wealthy. Anyone who enjoys entertaining guests in the comfort of their own home but doesn’t want the added burden of spending most of their time in the kitchen, may like to use the services of a personal chef. If you appreciate great food, enjoy personalised service, and want more time to enjoy entertaining your guests then our service is for you.</dd>

        <dt>Why choose our services rather than a restaurant?</dt>

        <dd>Very little beats the experience of eating in a wonderful restaurant when everything runs like clockwork BUT unfortunately a lot can and often does go wrong – from overly loud or annoying guests at a near table, to the dish you so wanted being sold out, to being sat in a draught or near to loos! When using our services none of these problems can arise and there are no time restrictions due to multiple restaurant sittings.</dd>

      </dl>

    </div>
  </div>
  
  <?php require_once('includes/footer.php'); ?>

  <?php require_once('includes/scripts.php'); ?>


  
</body>
</html>