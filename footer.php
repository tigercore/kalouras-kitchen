<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <!-- Use the .htaccess and remove these lines to avoid edge case issues.
       More info: h5bp.com/i/378 -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title></title>
  <meta name="description" content="">

  <!-- Mobile viewport optimized: h5bp.com/viewport -->
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->

  <link rel="stylesheet" href="css/style.css">

  <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->

  <!-- All JavaScript at the bottom, except this Modernizr build.
       Modernizr enables HTML5 elements & feature detects for optimal performance.
       Create your own custom Modernizr build: www.modernizr.com/download/ -->
  <script src="js/libs/modernizr-2.5.3.min.js"></script>
</head>
<body id="homepage">
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
  <header>

    <h2 id="logo">Kaloura's Kitchen</h2>

    <nav>
      <ul>
        <li><a href="#">About Us</a></li>
        <li><a href="#">Sample Menus</a></li>
        <li><a href="#">Testimonials</a></li>
        <li><a href="#">FAQ&rsquo;s</a></li>
        <li><a href="#">Prices</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
    </nav>
  </header>
  <div id="main" role="main">

    <div id="carousel">
      <div id="one"></div>
      <div id="two">
        <p>“Be a guest at your own dinner party. <br> Take the stress out of entertaining and let Georgie do the hard work”</p>
      </div>
      <div id="three">
        <p>“Our regularly changing menus make the most <br> of seasonal ingredients and freshly-caught Cornish fish”</p>
      </div>
      <div id="four">
        <p>“Your menu can be devised to suit your needs whether it is a particular dietary requirement<br>or maybe you just want a selection of canapés or chilled local seafood with a glass of bubbly”</p>
      </div>
    </div>

    <h3>Kaloura’s Kitchen is owned and run by Georgina Kaloura Stevens.</h3>

    <p>Whether you are just visiting or a resident in Cornwall why not treat yourself to a meal cooked the comfort of your own house or holiday home by a professional chef? Kaloura’s Kitchen will take the stress out of entertaining by preparing a beautiful meal in your kitchen and cleaning up afterwards leaving you time to spend with your guests. We offer many different menu options and styles, influenced mainly by modern Mediterranean, British and Asian recipes.  We can cater for three to five course dinners, brunch and lunch parties, canapé and cocktail parties, high tea and even a romantic dinner party for two. What better way to spoil that special person?</p>

    <p>The service is individually tailored to you, and you can be assured of a relaxing meal cooked with the freshest ingredients, sourcing highest quality local Cornish produce wherever possible. As well as providing the chef, we will provide waiting staff if required. When the meal is finished we will leave your kitchen just as we found it, with only the memory of a wonderful meal to remind you we were there.</p>


  </div>
  <footer>

  </footer>


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.min.js"><\/script>')</script>

  <!-- scripts concatenated and minified via build script -->
  <script src="js/plugins.js"></script>
  <script src="js/script.js"></script>


<script src="http://malsup.github.com/jquery.cycle.all.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#carousel').cycle({
    fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
  });
});
</script>
  <!-- end scripts -->

  <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID.
       mathiasbynens.be/notes/async-analytics-snippet -->
  <script>
    var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
    g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
    s.parentNode.insertBefore(g,s)}(document,'script'));
  </script>
</body>
</html>