<!doctype html>
<html class="no-js" lang="en">
<head>
  
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Kalouras' Kitchen</title>
  <meta name="description" content="Private catering in your own home by an internationally trained chef using local produce. Based in Cornwall.">
  
  <meta name="viewport" content="width=device-width">
  
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>

  <link rel="stylesheet" href="css/style.css">

  <script src="js/libs/modernizr-2.5.3.min.js"></script>
  <script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>

</head>