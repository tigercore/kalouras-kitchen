<header>
  <a href="/"><p id="logo">Kaloura's Kitchen</p></a>
  <nav>
    <ul>
      <li><a href="about.php">About Us</a></li>
      <li><a href="menus.php">Sample Menus</a></li>
      <li><a href="testimonials.php">Testimonials</a></li>
      <li><a href="faq.php">FAQ&rsquo;s</a></li>
      <li><a href="prices.php">Prices</a></li>
      <li><a href="contact.php">Contact</a></li>
    </ul>
  </nav>
</header>