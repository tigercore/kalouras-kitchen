/* Author: Ryan Kelly */

$(document).ready(function() {
	$('#carousel').cycle({
    	fx: 'fade' // choose your transition type, ex: fade, scrollUp, shuffle, etc...
  	});
    
    $("ul.tabs").tabs("div.panes > div");
});

