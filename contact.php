<?php require_once('includes/head.php'); ?>
<body id="contact">
  
<?php require_once('includes/header.php'); ?>
  <div id="main" role="main">

    <div id="content">



    <h2>Contact</h2>
      
      <img class="banner" src="img/contact.png">
      
      <p class="contact-email"><a href="mailto: georgie@kalouraskitchen.com">georgie@kalouraskitchen.com</a></p>

      <p class="phone-number">Tel: 07966 257971</p>

    </div>
  </div>
  
  <?php require_once('includes/footer.php'); ?>

  <?php require_once('includes/scripts.php'); ?>


  
</body>
</html>