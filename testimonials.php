<?php require_once('includes/head.php'); ?>
<body id="testimonials">
  
<?php require_once('includes/header.php'); ?>
  <div id="main" role="main">

    <div id="content">

    <h2>Testimonials</h2>

    <img class="banner" src="img/testimonials.png" >

      <dl>

        <dt>Thank you so much for coming to us for the weekend, can't believe how hard you worked to make sure every one of my guests had a fabulous time! The food was amazing and it was great for me to have time to socialise with my friends instead of shopping, cooking and cleaning all weekend! You've totally changed the way I think about having friends here for the weekend, from a chore to a mini holiday.</dt>
        <dd>Mrs Rogers, Dorset</dd>
        <dt>Fragrant and spicy, the meal was absolutely fantastic and I loved every course.</dt>
        <dd>James, Yorkshire</dd>
        <dt>An extraordinary evening, the best meal of my life - and it was at home and I didn't even have to do the washing up</dt>
        <dd>Maria, Surrey</dd>
        <dt>A delightful meal, prepared without fuss by a delightful girl.</dt>
        <dd>Patricia, Yorkshire</dd>
        <dt>Simply the best food that can be eaten in Padstow. Which is really saying something.</dt>
        <dd>Chris, Surrey</dd>
        <dt>An absolute delight! An imaginative and delicious menu wonderfully cooked.</dt>
        <dd>Simon and Julia, Herts.</dd>
        <dt>Thankyou for helping to make my 40th birthday so special- fantastic food and wonderful service.</dt>
        <dd>Fiona and Mick Ryder, Middlesex</dd>
      </dl>

    </div>
  </div>
  
  <?php require_once('includes/footer.php'); ?>

  <?php require_once('includes/scripts.php'); ?>


  
</body>
</html>