<?php require_once('includes/head.php'); ?>
<body id="homepage">
  
<?php require_once('includes/header.php'); ?>
  <div id="main" role="main">

    <div id="carousel">
      <div id="one"></div>
      <div id="two">
        <p>Be a guest at your own dinner party. <br> Take the stress out of entertaining and let Georgie do the hard work.</p>
      </div>
      <div id="three">
        <p>Our regularly changing menus make the most <br> of seasonal ingredients and freshly-caught Cornish fish.</p>
      </div>
      <div id="four">
        <p>Your menu can be devised to suit your needs <br> for a particular dietary requirement.</p>
      </div>
    </div>

    

    <h3>Private catering in your own home by an internationally trained chef using local produce</h3>

    <p>Whether you are just visiting or are resident in Cornwall why not treat yourself to a meal cooked in the comfort of your own house or holiday home by a professional chef? Kaloura’s Kitchen will take the stress out of entertaining, by preparing a beautiful meal in your kitchen and cleaning up afterwards, leaving you time to relax. We offer many different menu options and styles, influenced mainly by modern Mediterranean, British and Asian recipes.  We can cater for three to five course dinners, brunch and lunch parties, canapé and cocktail parties, high tea and even a romantic dinner for two. What better way to spoil that special person?</p>

    <p>The service is individually tailored to you, and you can be assured of a relaxing meal cooked with the freshest ingredients, sourcing highest quality local Cornish produce wherever possible. As well as providing the chef, we will provide waiting staff if required. When the meal is finished we will leave your kitchen just as we found it, with only the memory of a wonderful meal to remind you we were there.</p>


  </div>
  
  <?php require_once('includes/footer.php'); ?>

  <?php require_once('includes/scripts.php'); ?>


  
</body>
</html>