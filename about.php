<?php require_once('includes/head.php'); ?>
<body id="about">
  
<?php require_once('includes/header.php'); ?>
  <div id="main" role="main">

    <div id="content">

    <h2>About</h2>

      <img id="georgie" src="/img/georgie.png">

      <p>Kaloura’s Kitchen is owned and run by Georgina Kaloura Stevens. Georgie has had 15 years experience in the hospitality industry. After a short stint of work experience with Rick Stein at age 18 she knew she wanted to work for him. She studied Hospitality Management with Culinary Arts at Oxford Brookes University and spent her 15 months training at Rick Stein’s The Seafood Restaurant and St Petroc’s Bistro in Padstow. After university she returned to Padstow and worked at the renowned Margot’s Bistro.</p> 

      <p>Combining her love of cooking and her love of being on the water she spent 6 years working on superyachts throughout the Mediterranean. For four years of which she was the personal chef to a Russian Oligarch, having to constantly adapt her cooking style to satisfy the diverse demands of him, his family and his guests. Through her travels she picked up many influences especially from Italy, Greece and Croatia. Sailing as a private chef meant constantly changing sources of produce, one day a large Croatian port, the next a fishing village in Italy.  Such experience enables Georgie to offer a professional and discreet service. From a bespoke dinner party at home to catering on yachts, villas and chalets there is not a challenge she cannot meet.</p>
      
      <p>Georgie typically spends 2 months a year in Thailand as it is the home of her favourite cuisine. She also has a huge passion for patisserie and was lucky enough to have one on one training with Gael Majchrzak, Head pastry chef at Gil’s Dubrovnik.</p>

    </div>
  </div>
  
  <?php require_once('includes/footer.php'); ?>

  <?php require_once('includes/scripts.php'); ?>


  
</body>
</html>